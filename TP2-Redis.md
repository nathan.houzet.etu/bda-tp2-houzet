
# TP2 REDIS - LIGHTTPD

## NATHAN HOUZET GL G2

[Lien du git](https://gitlab.univ-lille.fr/nathan.houzet.etu/bda-tp2-houzet)
## Commandes utiles : 

### Pour VM REDIS

ssh ubuntu@172.28.101.90

ouvrir reddis depuis vm : redis-cli
dans redis : AUTH 1234
puis requetes normales

sudo nano /etc/redis/redis.conf 
sudo systemctl restart redis.service
sudo systemctl status redis

Pour tester la connexion distant avec redis : telnet 172.28.101.90 6379

# pour la VM Middleware

ssh ubuntu@172.28.101.82
curl 172.28.101.82/hello.cgi

## QUESTIONS :

À quoi sert la commande SELECT ? 
la commande select sert à changer de database.
En fesant select 1 on change de db de la db0 à la db1 et donc on ne retrouve effectivement pas le couple big_answer 42 car il est stocké dans la db0.  

### Les API REST 

Que retourne `curl $middleware_ip/env.cgi ?

```bash
SERVER_NAME=172.28.101.82
SCRIPT_NAME=/env.cgi
REDIRECT_STATUS=200
GATEWAY_INTERFACE=CGI/1.1
SERVER_SOFTWARE=lighttpd/1.4.55
DOCUMENT_ROOT=/var/www/html
PWD=/var/www/html
REQUEST_URI=/env.cgi
REQUEST_SCHEME=http
QUERY_STRING=
HTTP_ACCEPT=*/*
REMOTE_PORT=44854
HTTP_HOST=172.28.101.82
SERVER_ADDR=172.28.101.82
HTTP_USER_AGENT=curl/7.58.0
SHLVL=1
CONTENT_LENGTH=0
SERVER_PROTOCOL=HTTP/1.1
SERVER_PORT=80
SCRIPT_FILENAME=/var/www/html/env.cgi
REMOTE_ADDR=10.135.9.161
REQUEST_METHOD=GET
_=/usr/bin/printenv

```

Il s'agit des variables d'environnement de la vm middleware.

### Les API rest 

curl --data "Holla Mundo!" 172.28.101.82/env.cgi?halloWelt 

-> 

<SERVER_NAME=172.28.101.82
SCRIPT_NAME=/env.cgi
REDIRECT_STATUS=200
GATEWAY_INTERFACE=CGI/1.1
SERVER_SOFTWARE=lighttpd/1.4.55
DOCUMENT_ROOT=/var/www/html
PWD=/var/www/html
HTTP_CONTENT_LENGTH=12
REQUEST_URI=/env.cgi?halloWelt
REQUEST_SCHEME=http
QUERY_STRING=halloWelt
HTTP_ACCEPT=*/*
REMOTE_PORT=56392
HTTP_HOST=172.28.101.82
SERVER_ADDR=172.28.101.82
HTTP_USER_AGENT=curl/7.58.0
SHLVL=1
CONTENT_LENGTH=12
SERVER_PROTOCOL=HTTP/1.1
SERVER_PORT=80
SCRIPT_FILENAME=/var/www/html/env.cgi
REMOTE_ADDR=10.135.9.161
CONTENT_TYPE=application/x-www-form-urlencoded
REQUEST_METHOD=POST
_=/usr/bin/printenv
Holla Mundo!icare@icare-IV:~/Documents/M2S1/bdd/bdda-tp2

En conclure comment les données GET et POST sont transmises à l’application en CGI :

Les données des methodes POST sont transmises via l'entrée std stdin.

### Réecriture URL 

curl $middleware_ip/une/url/foireuse?avec=donnée

->

SERVER_NAME=172.28.101.82
SCRIPT_NAME=/env.cgi
REDIRECT_STATUS=200
GATEWAY_INTERFACE=CGI/1.1
SERVER_SOFTWARE=lighttpd/1.4.55
DOCUMENT_ROOT=/var/www/html
PWD=/var/www/html
REQUEST_URI=/une/url/foireuse?avec=donnée
REQUEST_SCHEME=http
QUERY_STRING=
HTTP_ACCEPT=*/*
REMOTE_PORT=57032
HTTP_HOST=172.28.101.82
SERVER_ADDR=172.28.101.82
HTTP_USER_AGENT=curl/7.58.0
SHLVL=1
CONTENT_LENGTH=0
SERVER_PROTOCOL=HTTP/1.1
SERVER_PORT=80
SCRIPT_FILENAME=/var/www/html/env.cgi
REMOTE_ADDR=10.135.9.161
REDIRECT_URI=/env.cgi
REQUEST_METHOD=GET
_=/usr/bin/printenv


En analysant le retour, indiquer quelle variable d’environnement stock l’URL initiale avec réécriture par lighttp:

<li> ici on redirige tout les requete vers le fichier /env.cgi : "url.rewrite-once = ( "^/(.*)" => "/env.cgi" ) " </li>

### CGI avec Python


Que ce passe t’il quand on consulte la page curl $middleware_ip/env ?


CONTENT_LENGTH:0
QUERY_STRING:
REQUEST_URI:/env
REDIRECT_URI:/env.py
REDIRECT_STATUS:200
SCRIPT_NAME:/env.py
SCRIPT_FILENAME:/var/www/html/env.py
DOCUMENT_ROOT:/var/www/html
REQUEST_METHOD:GET
SERVER_PROTOCOL:HTTP/1.1
SERVER_SOFTWARE:lighttpd/1.4.55
GATEWAY_INTERFACE:CGI/1.1
REQUEST_SCHEME:http
SERVER_PORT:80
SERVER_ADDR:172.28.101.82
SERVER_NAME:172.28.101.82
REMOTE_ADDR:10.135.9.161
REMOTE_PORT:57330
HTTP_HOST:172.28.101.82
HTTP_USER_AGENT:curl/7.58.0
HTTP_ACCEPT:*/*
LC_CTYPE:C.UTF-8

On obtient le meme resultat que si on avait requeté env.py avec la redirection env -> env.py

### Un intergiciel et Redis pour une petit application web

Concernant l'implémentation de l'API, je me suis arrété à la commande fournissant les informations sur la sessions en cours. ([ici les sources](/middleware))
Ayant un peu de mal à manipuler les methodes de requétage HTTP cela m'as déjà pris beaucoup de temps.

Vous trouverez ici les urls fonctionnelles.

``` bash
API : 

CREATE USER : curl -X POST -d "toto-toto" 172.28.101.82/create
CONNECT USER :  curl -X POST -d "toto-toto" 172.28.101.82/connect
HAVE INFO ABOUT SESSION : curl -v --cookie "sessionId=RECEIVED-TOKEN" 172.28.101.82/

```

Quand aux accès des VM , j'ai ajouté vos clés ssh sur les deux vm donc vous devriez normalement y avoir accès.

POur donner mon avis sur le TP, j'avais déjà entendu parler de la notion de middleware / progiciels lors de mon stage de L3 (j'ai été chargé de créer un script d'installation du progiciel oracle MDM )
Cependant je n'en avais encore jamais implémenté ni compris l'utilité et le fonctionnement exact.
Ce TP m'as bien aidé pour comprendre ces points.