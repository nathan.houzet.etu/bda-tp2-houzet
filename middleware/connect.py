from redis_connect import connection_redis
import uuid, sys, time, hashlib

# Get user name and pass
data=sys.stdin.readline().split("-")
username=data[0]
userPass=data[1]

# Check if user exists
r=connection_redis(1)
try :
    uid = r[username]
    r=connection_redis(2)
    hashPass = r.hget(uid, "pass")

    # Check if pass are equlas
    sha_obj = hashlib.sha256()
    sha_obj.update(userPass.encode('utf-8'))
    userPass = sha_obj.hexdigest()

    if userPass != hashPass.decode() :
        print("STATUS:401\n")
        print("Wrong password.")
        sys.exit(0)

    else :
        # Create token
        r.hset(uid, "last_connection", time.time())
        r=connection_redis(5)
        user_uuid = uuid.uuid4().hex

        r[user_uuid] = uid
        r.expire(user_uuid, 60*10)
        print("Set-Cookie: sessionId="+user_uuid+"\n")
        print("User connected. Session expire in 10 minutes.")

except KeyError:
    print("STATUS:404\n")
    print("User unknown.")
    sys.exit(0)

except Exception as e :
    print("unexpected error : " + str(e))