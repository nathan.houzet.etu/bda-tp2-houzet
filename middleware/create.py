from redis_connect import connection_redis
import uuid, hashlib, time, sys

# Get and check user name and pass
data=sys.stdin.read().split("-")
username=data[0]
userPass=data[1]

if username == None or username == "" :
    print("STATUS:406\n")
    print("Wrong format for user name.")
    sys.exit(0)
if userPass == None or userPass == "" :
    print("STATUS:406\n")
    print("Wrong format for password.")
    sys.exit(0)

# Check if user already exists
r=connection_redis(1)
try :
    r[username]
    print("STATUS:403\n")
    print("User already exists.")
    sys.exit(0)
except KeyError :
    pass

# Create user
userUuid = uuid.uuid4().hex
r[username] = userUuid

sha_obj = hashlib.sha256()
sha_obj.update(userPass.encode('utf-8'))
hash_pwd = sha_obj.hexdigest()

r = connection_redis(2)
r.hset(userUuid, "name", username)
r.hset(userUuid, "pass", hash_pwd)
r.hset(userUuid, "create_at", time.time())

print("USER CREATED.")