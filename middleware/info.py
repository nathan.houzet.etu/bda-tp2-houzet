from redis_connect import connection_redis
import os, sys

cookie = os.environ.get("HTTP_COOKIE")

if cookie == None :
    print("STATUS:200\n")
    sys.exit(0)

else :
    cookie = cookie.split("=")[1]
    r=connection_redis(5)
    try :
        uid = r[cookie]

    except KeyError :
        print("STATUS:401\n")
        print("Session has expired.")
        sys.exit(0)

    print("content-type: application/json\n")
    r=connection_redis(2)
    print(r.hgetall(uid))